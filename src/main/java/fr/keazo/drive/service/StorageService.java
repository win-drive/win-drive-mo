package fr.keazo.drive.service;

import fr.keazo.drive.configuration.FileConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@RequiredArgsConstructor
public class StorageService {

    private final UserService userService;
    private final FileConfiguration fileConfiguration;

    public boolean save(MultipartFile file) {
        if (file == null) return false;
        if (file.getOriginalFilename() == null) return false;

        try {
            String username = userService.getConnected().getUsername();
            Path directory = Paths.get(fileConfiguration.getDirectory()).resolve(username);
            Files.createDirectories(directory);
            Files.copy(file.getInputStream(), directory.resolve(file.getOriginalFilename()));
            return true;
        } catch (IOException e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    public Resource load(String filename) {
        try {
            String username = userService.getConnected().getUsername();
            Path directory = Paths.get(fileConfiguration.getDirectory()).resolve(username);
            Path file = directory.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

}

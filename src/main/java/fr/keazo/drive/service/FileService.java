package fr.keazo.drive.service;

import fr.keazo.drive.configuration.FileConfiguration;
import fr.keazo.drive.model.File;
import fr.keazo.drive.model.Node;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FileService {

    private final UserService userService;
    private final FileConfiguration fileConfiguration;

    public Node getTree() {
        String username = userService.getConnected().getUsername();
        Path directory = Paths.get(fileConfiguration.getDirectory(), username);

        try {
            Files.createDirectories(directory);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Node.builder()
                .isDirectory(true)
                .children(getAllByPath(directory))
                .build();
    }

    private List<Node> getAllByPath(Path path) {
        try {
            return Files.walk(path, 1).filter(filePath -> !filePath.toString().equals(path.toString())).map(filePath -> {
                java.io.File file = filePath.toFile();

                return Node.builder()
                        .name(file.getName())
                        .isDirectory(file.isDirectory())
                        .children(getAllByPath(filePath))
                        .build();
            }).toList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public List<File> getAll() {
        try {
            String username = userService.getConnected().getUsername();
            Path directory = Paths.get(fileConfiguration.getDirectory(), username);

            Files.createDirectories(directory);

            return Files.walk(directory).filter(filePath -> !filePath.toString().equals(directory.toString())).map(filePath -> {
                java.io.File file = filePath.toFile();
                try {
                    BasicFileAttributes attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
                    LocalDateTime fileTime = LocalDateTime.ofInstant(attributes.lastModifiedTime().toInstant(), ZoneId.systemDefault());

                    return File.builder()
                            .name(file.getName())
                            .lastModified(fileTime)
                            .size(attributes.size())
                            .isDirectory(file.isDirectory())
                            .path(directory.relativize(file.toPath().getParent()).toString())
                            .build();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }).toList();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

}

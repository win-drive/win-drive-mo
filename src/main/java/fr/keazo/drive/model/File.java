package fr.keazo.drive.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class File {

    private String name;

    private LocalDateTime lastModified;

    private Long size;

    private Boolean isDirectory;

    private String path;

}

package fr.keazo.drive.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class Node {

    private String name;

    private Boolean isDirectory;

    @Builder.Default
    private List<Node> children = new ArrayList<>();

}

package fr.keazo.drive.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("files")
@Getter
@Setter
public class FileConfiguration {

    private String directory;

}

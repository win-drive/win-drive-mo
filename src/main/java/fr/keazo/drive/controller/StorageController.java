package fr.keazo.drive.controller;

import com.sun.istack.NotNull;
import fr.keazo.drive.service.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/storage")
@RequiredArgsConstructor
public class StorageController {

    private final StorageService storageService;

    @GetMapping("/download")
    public Resource download(@RequestParam @NotNull String path) {
        return storageService.load(path);
    }

    @PostMapping("/upload")
    public Boolean upload(@RequestBody @NotNull MultipartFile multipartFile) {
        return storageService.save(multipartFile);
    }

}

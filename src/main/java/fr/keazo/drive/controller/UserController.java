package fr.keazo.drive.controller;

import fr.keazo.drive.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/signup")
    public boolean signup(@RequestParam String username, @RequestParam String password) {
        return userService.add(username, password);
    }

}

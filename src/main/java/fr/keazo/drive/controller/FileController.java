package fr.keazo.drive.controller;

import fr.keazo.drive.model.File;
import fr.keazo.drive.model.Node;
import fr.keazo.drive.service.FileService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/files")
@RequiredArgsConstructor
public class FileController {

    private final FileService fileService;

    @GetMapping("/tree")
    public Node getTree() {
        return fileService.getTree();
    }

    @GetMapping()
    public List<File> getAll() {
        return fileService.getAll();
    }

}

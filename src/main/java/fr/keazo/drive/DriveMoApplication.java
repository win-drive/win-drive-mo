package fr.keazo.drive;

import fr.keazo.drive.configuration.FileConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(FileConfiguration.class)
public class DriveMoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DriveMoApplication.class, args);
    }

}
